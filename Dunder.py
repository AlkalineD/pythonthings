class test:

    # init is used in initializing a class
    def __init__(self, thing, second_thing, third_thing):
        self.thing = thing
        self.second_thing = second_thing
        self.third_thing = third_thing

    def __int__(self):
        self.thing = ""

    # repr is used to customize what will be printed when an object is to be printed, if repr method is absent,
    # address of the object will be printed
    def __repr__(self):
        return f'{self.third_thing}, {self.thing}, {self.second_thing}'

if __name__ == '__main__':
    t = test("Hello", "Second", "third")
    print(repr(t))

    # t1 = test()
    # print(t1)